import { roundByRate } from './index';

describe('round to rate', () => {
  it('rounds regular', () => {
    expect(roundByRate(22, 5)).toEqual(25);
    expect(roundByRate(122, 50)).toEqual(150);
    expect(roundByRate(82, 10)).toEqual(90);
    expect(roundByRate(1451, 500)).toEqual(1500);
    expect(roundByRate(2023, 1000)).toEqual(3000);
  });

  it('rounds small', () => {
    expect(roundByRate(0, 5)).toEqual(0);
    expect(roundByRate(1, 50)).toEqual(50);
    expect(roundByRate(1, 0)).toBeNaN();
  });

  it('rounds floats', () => {
    expect(roundByRate(44.234, 40)).toEqual(80);
  });
});
