export function roundByRate(value: number, rate: number): number {
  return Math.ceil(value / rate) * rate;
}
