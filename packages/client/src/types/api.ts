import { CurrencyItem } from '@/types/index';

export type Api = {
  payments: PaymentsApi;
};

export type PaymentsApi = {
  donate: (amount: number, currency: CurrencyItem['code']) => Promise<void>;
};
