export type CurrencyItem = {
  name: string;
  code: string;
  symbol: string;
  rate: number;
};
