import {convertCurrency, roundByRules} from './index';

describe('currency converter', () => {
  it('regular converts', () => {
    expect(convertCurrency(100, 'USD', 'RUB')).toEqual(6346.1993);
    expect(convertCurrency(100, 'EUR', 'RUB')).toEqual(7070.210016299075);
    expect(convertCurrency(897597, 'EUR', 'USD')).toEqual(1000000);
    expect(convertCurrency(6346.1993, 'RUB', 'USD')).toEqual(100);
  });

  it('processes zeros', () => {
    expect(convertCurrency(0, 'USD', 'RUB')).toEqual(0);
  });

  it('processes negatives', () => {
    expect(convertCurrency(-100, 'USD', 'RUB')).toEqual(-6346.1993);
  });

  it('processes same currency', () => {
    expect(convertCurrency(100, 'USD', 'USD')).toEqual(100);
    expect(convertCurrency(99.999, 'USD', 'USD')).toEqual(99.999);
  });

  it('processes big numbers', () => {
    expect(convertCurrency(1e24, 'USD', 'RUB')).toEqual(6.3461993e25);
    expect(convertCurrency(999999, 'USD', 'RUB')).toEqual(63461929.538007);
  });

  it('processes floats', () => {
    expect(convertCurrency(1 / 3, 'USD', 'RUB')).toEqual(21.153997666666665);
  });
});

describe('round to rate by rules', () => {
  it('rounds regular', () => {
    expect(roundByRules(22)).toEqual(25);
    expect(roundByRules(122)).toEqual(150);
    expect(roundByRules(82)).toEqual(90);
    expect(roundByRules(1451)).toEqual(1500);
    expect(roundByRules(2023)).toEqual(2250);
    expect(roundByRules(3123)).toEqual(3500);
    expect(roundByRules(4235)).toEqual(4500);
    expect(roundByRules(4820)).toEqual(5000);
    expect(roundByRules(8820)).toEqual(9000);
  });

  it('rounds small', () => {
    expect(roundByRules(0)).toEqual(0);
    expect(roundByRules(1)).toEqual(5);
  });

  it('rounds floats', () => {
    expect(roundByRules(44.234)).toEqual(45);
  });
});
