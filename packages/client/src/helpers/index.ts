import { CurrencyItem } from '@/types';
import { currencies } from '@/constants';
import { roundByRate } from '@/utils';

export function getCurrConvertMap(
  currList: CurrencyItem[]
): Record<CurrencyItem['code'], CurrencyItem['rate']> {
  return currList.reduce(
    (acc, { code, rate }) => ({ ...acc, [code]: rate }),
    {}
  );
}

export function convertCurrency(
  amount: number,
  fromCurrency: CurrencyItem['code'],
  toCurrency: CurrencyItem['code']
): number {
  const map = getCurrConvertMap(currencies);
  const fromRate = map[fromCurrency];
  const toRate = map[toCurrency];

  return (amount * toRate) / fromRate;
}

export type RoundRule = {
  value: number;
  checker: (amount: number) => boolean;
};

export function roundByRules(amount: number): number {
  const roundingRules: RoundRule[] = [
    {
      value: 5,
      checker: (amount) => amount < 50,
    },
    {
      value: 10,
      checker: (amount) => amount < 100,
    },
    {
      value: 50,
      checker: (amount) => amount < 350,
    },
    {
      value: 100,
      checker: (amount) => amount < 1500,
    },
    {
      value: 250,
      checker: (amount) => amount < 2500,
    },
    {
      value: 500,
      checker: (amount) => amount < 5000,
    },
    {
      value: 1000,
      checker: (amount) => amount < 10000,
    },
    {
      value: 5000,
      checker: () => true,
    },
  ];

  for (const { value, checker } of roundingRules) {
    if (checker(amount)) return roundByRate(amount, value);
  }

  return amount;
}

export function getCurrencyData(
  currencyCode: CurrencyItem['code']
): CurrencyItem {
  return currencies.find(({ code }) => code === currencyCode) as CurrencyItem;
}
