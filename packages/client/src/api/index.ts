import { Api } from '@/types/api';
import { http } from '@/api/http';

export const api: Api = {
  payments: {
    donate(amount, currency) {
      return http.post('/donate', { amount, currency });
    },
  },
};
