type URL = string;
type Body = Record<string, unknown>;

type ReqMethodAlias = 'get' | 'post' | 'put' | 'patch' | 'delete';

type Http = Record<
  ReqMethodAlias,
  <R = void>(url: URL, body: Body) => Promise<R>
>;

async function request<R = void>(
  url: URL,
  method: string,
  body?: Body
): Promise<R> {
  const res = await fetch(url, {
    method,
    body: JSON.stringify(body),
    headers: {
      'Content-Type': 'application/json',
    },
  });

  if (res.ok) return res.json();

  const err = await res.text();
  throw err;
}

export const http: Http = {
  get: <R = void>(url: URL, body: Body): Promise<R> =>
    request(url, 'GET', body),
  post: <R = void>(url: URL, body: Body): Promise<R> =>
    request(url, 'POST', body),
  put: <R = void>(url: URL, body: Body): Promise<R> =>
    request(url, 'PUT', body),
  patch: <R = void>(url: URL, body: Body): Promise<R> =>
    request(url, 'PATCH', body),
  delete: <R = void>(url: URL, body: Body): Promise<R> =>
    request(url, 'DELETE', body),
};
