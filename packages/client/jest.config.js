module.exports = {
  preset: '@vue/cli-plugin-unit-jest/presets/typescript-and-babel',
  // below 'vue' should be on the top, it isn't clear but help to solve some strange issues in tests
  // https://github.com/vuejs/vue-test-utils/issues/1779
  moduleFileExtensions: ['vue', 'ts', 'tsx', 'js', 'jsx', 'json'],
  transform: {
    '^.+\\.js$': ['babel-jest', { cwd: __dirname }],
    '^.+\\.tsx?$': ['ts-jest', { cwd: __dirname }],
    '^.*\\.(vue)$': 'vue-jest',
    '.+\\.(css|styl|less|sass|scss|png|jpg|ttf|woff|woff2)$':
      'jest-transform-stub',
  },
  collectCoverage: false,
  testMatch: ['**/?(*.)+(spec|test).[jt]s?(x)'],
  testPathIgnorePatterns: ['/node_modules/'],
  globals: {
    'ts-jest': {
      diagnostics: false,
    },
  },
  maxWorkers: 1,
};
