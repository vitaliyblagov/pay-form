module.exports = {
  css: {
    loaderOptions: {
      scss: {
        additionalData: `
        @import "@/assets/style/vars.scss";
        @import "@/assets/style/global.scss";
        @import "@/assets/style/functions.scss";
        `,
      },
    },
  },
  devServer: {
    proxy: 'http://localhost:3000',
  },
};
