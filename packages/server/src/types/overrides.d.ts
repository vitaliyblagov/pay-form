export {};

declare global {
  namespace NodeJS {
    interface ProcessEnv {
      DB_CONN_STRING: string;
      DONATIONS_COLLECTION_NAME: string;
    }
  }
}
