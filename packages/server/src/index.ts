import express from 'express';
import { connectToDB } from './services/database.service';
import { donationsRouter } from './routes/donations.router';
import { DEFAULT_APP_HOST, DEFAULT_APP_PORT } from './constants';

async function main(): Promise<void> {
  try {
    const PORT = Number(process.env.PORT || DEFAULT_APP_PORT);
    const HOST = process.env.HOST || DEFAULT_APP_HOST;

    await connectToDB();
    const app = express();

    app.use(express.static('../client/dist'));
    app.use('/', donationsRouter);
    app.listen(PORT, HOST, () => {
      console.log(`Server started on http://${HOST}:${PORT}`);
    });
  } catch (e) {
    console.error(e);
  }
}

main();
