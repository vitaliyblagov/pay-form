import { Document, Schema, model } from 'mongoose';
import { CurrencyCode } from '../types/donation';
import { DEFAULT_COLLECTION_NAME } from '../constants';

export interface IDonation extends Document {
  amount: number;
  currency: CurrencyCode;
}

export const DonationSchema = new Schema({
  amount: {
    type: Number,
    required: [true, 'Amount is required'],
    min: [1, 'Minimum amount is 1'],
  },
  currency: {
    type: String,
    required: [true, 'Currency is required'],
    enum: {
      values: ['USD', 'EUR', 'GBP', 'RUB'],
      message: 'Currency is not supported',
    },
  },
});

const COLLECTION_NAME =
  process.env.DONATIONS_COLLECTION_NAME || DEFAULT_COLLECTION_NAME;

export const Donation = model<IDonation>(COLLECTION_NAME, DonationSchema);
