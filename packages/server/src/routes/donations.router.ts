import { Router, json } from 'express';
import * as Mongoose from 'mongoose';
import { Donation, IDonation } from '../models/donation';

export const donationsRouter = Router();
donationsRouter.use(json());

donationsRouter.get('/donations', async (req, res) => {
  try {
    const donations = await Donation.find();
    res.json(donations);
  } catch (e) {
    res.status(500).send(e?.message);
  }
});

donationsRouter.post('/donate', async (req, res) => {
  try {
    const donationPayload = req.body as IDonation;
    const donation = new Donation(donationPayload);
    const error = donation.validateSync();

    if (error)
      return res
        .status(400)
        .json({ ok: false, error: formatErrors(error.errors).join('; ') });

    await donation.save();

    return res.status(201).json({ ok: true });
  } catch (e) {
    res.status(400).json({
      ok: false,
      message: e,
    });
  }
});

function formatErrors(
  errors: Mongoose.Error.ValidationError['errors']
): string[] {
  return Object.values(errors).map(({ message }) => message);
}
