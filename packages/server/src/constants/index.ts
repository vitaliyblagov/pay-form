export const DEFAULT_COLLECTION_NAME = 'donations';

export const DEFAULT_DB_CONN_STRING = 'mongodb://127.0.0.1:27017/paymentsDB';

export const DEFAULT_APP_PORT = 3000;

export const DEFAULT_APP_HOST = '0.0.0.0';
