import { connect } from 'mongoose';
import { DEFAULT_DB_CONN_STRING } from '../constants';

export async function connectToDB(): Promise<void> {
  const CONN_STRING = process.env.DB_CONN_STRING || DEFAULT_DB_CONN_STRING;

  try {
    await connect(CONN_STRING);
    console.log('Successfully connected to database');
  } catch (e) {
    console.error(e);
  }
}
