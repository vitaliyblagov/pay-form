# Pay form test
Pay form with currency switch and amounts' recommendations.

## Install dependencies
```
$ yarn install
```

## Run in dev mode
```
$ yarn run dev
```

## Build
```
$ yarn run build
```

## Start production build
```
$ yarn run start
```

## Environment Variables
You need to set up following env variables to launch the app correctly:
- `DB_CONN_STRING` - connection string to Mongo DB cluster, for example `mongodb://127.0.0.1:27017/paymentsDB`
- `DONATIONS_COLLECTION_NAME` - collection's name

Default values are:
```
DEFAULT_COLLECTION_NAME = 'donations'
DEFAULT_DB_CONN_STRING = 'mongodb://127.0.0.1:27017/paymentsDB'
```
